import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ApiRestService {

  URL = `https://pokeapi.co/api/v2`;

  detallePokemon;

  constructor(
    private http: HttpClient
  ) { }


  obtenerPokemon(posicion: number, limite:number) {
    return this.http.get(`${this.URL}/pokemon?offset=${posicion}&limit=${limite}`);
  }

  async getPokemon(url: string): Promise<any> {
    return this.http.get<any>(`${url}`).toPromise();
  }





  setDetallePokemon(detalle) {
    this.detallePokemon = detalle;
  }
  getDetallePokemon() {
    	return this.detallePokemon ? this.detallePokemon  : null;
  }




}
