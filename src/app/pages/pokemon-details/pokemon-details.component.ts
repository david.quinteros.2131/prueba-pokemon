import { ApiRestService } from './../../service/api-rest.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-pokemon-details',
  templateUrl: './pokemon-details.component.html',
  styleUrls: ['./pokemon-details.component.css']
})
export class PokemonDetailsComponent implements OnInit {
  detallePokemon: any;
  constructor(
    private servicio: ApiRestService,
    private router: Router

  ) {
    console.log('this.getDetallePokemon :>> ', this.servicio.getDetallePokemon());
    if(this.servicio.getDetallePokemon()) {
      this.detallePokemon = this.servicio.getDetallePokemon();
    } else {
      this.router.navigateByUrl('pokemon');

    }
   }

  ngOnInit(): void {

  }

}
