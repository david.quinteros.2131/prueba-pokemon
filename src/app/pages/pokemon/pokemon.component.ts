import { ApiRestService } from './../../service/api-rest.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.css']
})
export class PokemonComponent implements OnInit {

  page = 1;
  posicion: number = 0;
  limite: number = 100;
  pokemon: any = {};
  constructor(
    private servicio :ApiRestService,
    private router: Router
  ) {
    this.servicio.obtenerPokemon(this.posicion,this.limite).subscribe(
     res => {
        let pokemon = res['results'];
        for (let i = 0; i < pokemon.length; i++) {
           this.obtenerPokemon(pokemon[i].url).then(value=> {
            pokemon[i]['details'] = value;
            pokemon[i]['id'] = value.id;
          }).catch(err => {
            console.log('err :>> ', err);
          });
        }
        this.pokemon['results'] = pokemon;
        this.pokemon['count'] = res['count'];
        this.pokemon['next'] = res['next'];
        this.pokemon['previous'] = res['previous'];

      },
      err => {
        console.log('err :>> ', err);
      }
    );


  }

  ngOnInit(): void {

  }


  async obtenerPokemon(url): Promise<any> {
    return await this.servicio.getPokemon(url);;
  }

  refreshCountries() {
    this.posicion = this.limite * (this.page - 1)
    this.servicio.obtenerPokemon(this.posicion,this.limite).subscribe(
      res => {
        let pokemon = res['results'];
        for (let i = 0; i < pokemon.length; i++) {
          this.obtenerPokemon(pokemon[i].url).then(value=> {
            pokemon[i]['details'] = value;
            pokemon[i]['id'] = value.id;
          }).catch(err => {
            console.log('err :>> ', err);
          });
        }
        this.pokemon['results'] = pokemon;
        this.pokemon['count'] = res['count'];
        this.pokemon['next'] = res['next'];
        this.pokemon['previous'] = res['previous'];

      },
      err => {
        console.log('err :>> ', err);
      }
    );

  }


  navegarDetalle(detalle){
    this.servicio.setDetallePokemon(detalle)
    this.router.navigateByUrl('pokemon-details');
  }





}
